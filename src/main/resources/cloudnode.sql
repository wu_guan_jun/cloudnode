SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_note
-- ----------------------------
DROP TABLE IF EXISTS `tb_note`;
CREATE TABLE `tb_note` (
  `noteId` int NOT NULL AUTO_INCREMENT COMMENT '主键，自增',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `typeId` int DEFAULT NULL COMMENT '外键，从属tb_note_type',
  `pubTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间',
  PRIMARY KEY (`noteId`),
  KEY `fk_note_ref_type` (`typeId`),
  CONSTRAINT `fk_note_ref_type` FOREIGN KEY (`typeId`) REFERENCES `tb_note_type` (`typeId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (1, '上海一日游', '<p><strong>这是测试内容</strong></p>', 2, '2023-03-02 05:10:35');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (2, '第一次测试', '<p>测试</p>', 4, '2023-03-03 05:05:54');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (3, '今日工作安排', '<p>测试</p>', 1, '2023-03-03 05:05:51');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (4, '第二次测试', '<p>测试</p>', 1, '2023-03-03 05:05:50');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (5, '第三次测试', '<p>测试</p>', 1, '2023-03-03 05:05:47');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (6, '第四次测试', '<p>测试</p>', 1, '2023-03-03 05:02:21');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (7, '出行计划', '<p>出行计划</p>', 2, '2023-03-03 05:02:24');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (8, '测试001', '<p>测试001</p>', 4, '2023-03-03 05:02:26');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (9, '测试002', '<p>测试002</p>', 4, '2023-03-04 05:02:28');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (11, '重复', '<p>重复</p>', 3, '2023-03-04 02:22:49');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (12, '测试地址', '<p>测试</p>', 1, '2023-03-05 03:42:18');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (13, '测试003', '<p>测试003</p>', 1, '2023-03-04 03:45:32');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (14, '测试004', '<p>测试004</p>', 1, '2023-03-05 03:52:25');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (16, '测试', '<p><img src=\"http://img.baidu.com/hi/tsj/t_0001.gif\"/><img src=\"http://img.baidu.com/hi/tsj/t_0013.gif\"/></p><p><img src=\"/static/ueditor/jsp/upload/image/20230323/1679572666259029538.jpg\" title=\"1679572666259029538.jpg\"/></p><p><img src=\"/static/ueditor/jsp/upload/image/20230323/1679572666255022015.jpg\" title=\"1679572666255022015.jpg\"/></p><p><img src=\"/static/ueditor/jsp/upload/image/20230323/1679572666271016136.jpg\" title=\"1679572666271016136.jpg\"/></p><p><img src=\"/static/ueditor/jsp/upload/image/20230323/1679572666314063644.png\" title=\"1679572666314063644.png\"/></p><p></p><p></p>', 1, '2023-03-23 19:54:04');
INSERT INTO `tb_note`(`noteId`, `title`, `content`, `typeId`, `pubTime`) VALUES (17, '写论文', '<p>今天写了一千五百字，很不错。</p>', 1, '2023-03-24 13:45:53');




DROP TABLE IF EXISTS `tb_note_type`;
CREATE TABLE `tb_note_type` (
  `typeId` int NOT NULL AUTO_INCREMENT COMMENT '主键，自动增长',
  `typeName` varchar(50) NOT NULL COMMENT '类别名，在同一个用户下唯一',
  `userId` int DEFAULT NULL COMMENT '从属用户',
  PRIMARY KEY (`typeId`),
  KEY `fk_type_ref_user` (`userId`),
  CONSTRAINT `fk_type_ref_user` FOREIGN KEY (`userId`) REFERENCES `tb_user` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `tb_note_type`(`typeId`, `typeName`, `userId`) VALUES (1, '私人', 1);
INSERT INTO `tb_note_type`(`typeId`, `typeName`, `userId`) VALUES (2, '旅游', 1);
INSERT INTO `tb_note_type`(`typeId`, `typeName`, `userId`) VALUES (3, '美食', 1);
INSERT INTO `tb_note_type`(`typeId`, `typeName`, `userId`) VALUES (4, '测试', 1);



DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `userId` int NOT NULL AUTO_INCREMENT COMMENT '主键，自动增长',
  `uname` varchar(50) NOT NULL COMMENT '用户名',
  `upwd` varchar(50) DEFAULT NULL COMMENT '密码',
  `nick` varchar(50) DEFAULT NULL COMMENT '昵称',
  `head` varchar(100) DEFAULT NULL COMMENT '头像',
  `mood` varchar(500) DEFAULT NULL COMMENT '心情',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `tb_user`(`userId`, `uname`, `upwd`, `nick`, `head`, `mood`) VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'lisi', '001.jpg', '美滋滋');
INSERT INTO `tb_user`(`userId`, `uname`, `upwd`, `nick`, `head`, `mood`) VALUES (2, 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', 'zhangsan', 'jay.jpg', 'Hello');





