package com.jun.pojo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @title:Note
 * @Author Jun
 * @Date:2023/2/2 18:12
 * @Version 1.0
 */
@Getter
@Setter
public class Note {
    private Integer noteId; // 云记id
    private String title; // 云记标题
    private String content; // 云记内容
    private Integer typeId; // 云记类型
    private Date pubTime; // 发布时间 ---------- 注意这里的类型 Date


    private String typeName; // 云记类型名
}
