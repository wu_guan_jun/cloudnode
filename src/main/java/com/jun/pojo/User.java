package com.jun.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * @title:User
 * @Author Jun
 * @Date:2023/1/7 19:29
 * @Version 1.0
 */
@Getter
@Setter
public class User {

    private Integer userId; // 用户ID
    private String uname; // 用户姓名
    private String upwd; // 用户密码
    private String nick; // 用户昵称
    private String head; // 用户头像
    private String mood; // 用户签名

    public User(Integer userId, String uname, String upwd, String nick, String head, String mood) {
        this.userId = userId;
        this.uname = uname;
        this.upwd = upwd;
        this.nick = nick;
        this.head = head;
        this.mood = mood;
    }

    public User() {
    }
}
