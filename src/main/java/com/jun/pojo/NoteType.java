package com.jun.pojo;

import lombok.Getter;
import lombok.Setter;

/**
 * @title:NoteType
 * @Author Jun
 * @Date:2023/1/15 22:02
 * @Version 1.0
 */
@Getter
@Setter
public class NoteType {
    private Integer typeId; //类型ID
    private String typeName;    //类型名称
    private Integer userId;     //用户ID

}
