package com.jun.web;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.jun.pojo.NoteType;
import com.jun.pojo.User;
import com.jun.service.NoteTypeService;
import com.jun.utils.JsonUtils;
import com.jun.vo.ResultInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @title:NoteTypeServlet
 * @Author Jun
 * @Date:2023/1/15 22:07
 * @Version 1.0
 */
@WebServlet("/type")
public class NoteTypeServlet extends HttpServlet {

    private NoteTypeService noteTypeService = new NoteTypeService();

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置首页导航高亮
        request.setAttribute("menu_page", "type");
        //得到用户行为
        String actionName = request.getParameter("actionName");
        //判断用户行为
        if ("list".equals(actionName)) {
            //查询类型列表
            typeList(request, response);

        } else if ("delete".equals(actionName)) {
            //删除类型列表
            deleteType(request, response);
        } else if ("addOrUpdate".equals(actionName)) {
            //添加或者修改类型
            addOrUpdate(request, response);
        }
    }

    /**
     * 添加或修改云日记类型
     * @param request
     * @param response
     */
    private void addOrUpdate(HttpServletRequest request, HttpServletResponse response) {
        //1.接收参数（类型名称、类型ID）
        String typeName = request.getParameter("typeName");
        String typeId = request.getParameter("typeId");
        //2.获取session作用域中的user对象，得到用户ID
        User user = (User) request.getSession().getAttribute("user");
        //3.调用service层的更新方法，返回ResultInfo对象
        ResultInfo<Integer> resultInfo = noteTypeService.addOrUpdate(typeName, user.getUserId(), typeId);
        //4.将ResultInfo转换成JSON格式的字符串，响应给ajax的回调函数
        JsonUtils.toJson(response, resultInfo);
    }
    /**
     * 删除云日记类型
     * @param request
     * @param response
     */
    private void deleteType(HttpServletRequest request, HttpServletResponse response){
        //1.接收参数
        String typeId = request.getParameter("typeId");
        //2.返回service的更新操作，返回ResultInfo对象
        ResultInfo<NoteType> resultInfo = noteTypeService.deleteType(typeId);
        //3.将ResultInfo对象转换成json格式的字符串，响应给ajax的回调函数
        JsonUtils.toJson(response, resultInfo);
    }

    /**
     * 查询云日记类型列表
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void typeList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取session作用域
        User user = (User) request.getSession().getAttribute("user");
        //2.调用Service层查询方法，查询当前登录用户的类型集合，返回集合
        List<NoteType> typeList = noteTypeService.findTypeByUserId(user.getUserId());
        //3.将类型列表设置到作用域中
        request.setAttribute("typeList", typeList);
        //4.设置首页动态包含的页面值
        request.setAttribute("changePage", "type/list.jsp");
        //5.请求跳转到index.jsp页面
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
