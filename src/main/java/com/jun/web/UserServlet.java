package com.jun.web;

import com.jun.pojo.User;
import com.jun.service.UserService;
import com.jun.vo.ResultInfo;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * @title:UserServlet
 * @Author Jun
 * @Date:2023/1/7 20:41
 * @Version 1.0
 */
@WebServlet("/user")
@MultipartConfig
public class UserServlet extends HttpServlet {
    UserService userService = new UserService();

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置首页导航高亮
        request.setAttribute("menu_page", "user");
        // 接收用户的行为 ----------------
        String actionName = request.getParameter("actionName"); // 参数与<input>标签的name属性一致
        // 一个标志，解决个人中心修改提交后，刷新页面出现空白的问题
        String updateUserFlag = request.getParameter("updateUserActionName"); // 这是从input标签提交上来的参数
        if ("register".equals(actionName)) {
            //用户注册
            registerUser(request, response);
        } else if ("login".equals(actionName)) {
            //用户登录
            userLogin(request, response);
        } else if ("logout".equals(actionName)) {
            //用户退出
            userLogout(request, response);
        } else if ("userCenter".equals(actionName) && ("".equals(updateUserFlag) || updateUserFlag == null)) {
            //进入个人中心
            userCenter(request, response);
        } else if ("userHead".equals(actionName)) {
            //加载头像
            userHead(request, response);
        } else if ("checkNick".equals(actionName)) {
            checkNick(request, response);
        } else if ("userCenter".equals(actionName) && "updateUser".equals(updateUserFlag)) {
            //修改用户信息
            updateUser(request, response);
        } else if ("forget".equals(actionName)) {
            //忘记密码
            forget(request, response);
        }

    }

    /**
     * 用户忘记密码
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void forget(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ResultInfo<User> resultInfo = userService.forgetUser(request);
        if (resultInfo.getCode() == 1) {
            response.sendRedirect("login.jsp");
        } else {
            // 将resultInfo对象设置到request作用域中
            request.setAttribute("resultInfo", resultInfo.getMsg());
            // 请求转发跳到忘记密码页面
            request.getRequestDispatcher("forget.jsp").forward(request, response);
        }
    }

    /**
     * 修改用户信息
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void updateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //调用service方法，传递request对象作为参数，返回resultInfo对象
        ResultInfo<User> resultInfo = userService.updateUser(request);
        //将resultInfo对象存到request作用域中
        request.setAttribute("resultInfo", resultInfo);
        //请求转发到个人中心页
        request.getRequestDispatcher("user?actionName=userCenter&updateUserActionName=").forward(request, response);
    }
    /**
     * 验证昵称的唯一性
     * @param request
     * @param response
     * @throws IOException
     */
    private void checkNick(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //获取参数
        String nick = request.getParameter("nick");
        //从session作用域中获取用户对象，得到用户id
        User user = (User) request.getSession().getAttribute("user");
        //调用service层的方法，得到返回的结果
        Integer code = userService.checkNick(nick, user.getUserId());
        //通过字符流将结果响应给前台的ajax的回调函数
        response.getWriter().write(code + "");
        //关闭资源
        response.getWriter().close();
    }
    /**
     * 加载用户头像
     * @param request
     * @param response
     * @throws IOException
     */
    private void userHead(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // 1. 获取参数 （图片名称），是从user对象的head字段中取的
        String head = request.getParameter("imageName");
        // 2. 得到图片的存放路径 （得到项目的真实路径：request.getServletContext().getealPathR("/")）
        String realPath = request.getServletContext().getRealPath("/WEB-INF/upload");
        // 3. 通过图片的完整路径，得到file对象
        File file = new File(realPath + "/" + head);
        // 4. 通过截取，得到图片的后缀，从最后一个 . 开始截取，不包含 . 在内，所以 +1
        String pic = head.substring(head.lastIndexOf(".") + 1);
        // 5. 通过不同的图片后缀，设置不同的响应的类型
        if ("PNG".equalsIgnoreCase(pic)) {
            response.setContentType("image/png");
        } else if ("JPG".equalsIgnoreCase(pic) || "JPEG".equalsIgnoreCase(pic)) {
            response.setContentType("image/jpeg");
        } else if ("GIF".equalsIgnoreCase(pic)) {
            response.setContentType("image/gif");
        }
        // 6. 利用org.apache.commons.io.FileUtils的copyFile()方法，将图片拷贝给浏览器
        FileUtils.copyFile(file, response.getOutputStream());

    }
    /**
     * 进入个人中心
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void userCenter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置首页动态包含的页面值
        request.setAttribute("changePage", "user/info.jsp");
        //请求转发到index
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    /**
     * 用户退出
     * @param request
     * @param response
     * @throws IOException
     */
    private void userLogout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //销毁session对象
        request.getSession().invalidate();
        //重定向跳转到登录页面
        response.sendRedirect("login.jsp");
    }
    /**
     * 用户注册
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void registerUser(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

        //接收信息
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        request.setAttribute("username", username);
        request.setAttribute("password", password);
        ResultInfo<User> resultInfo = userService.userRegister(username, password);
        if (resultInfo.getCode() == 1) {
            response.sendRedirect("login.jsp");
        } else {
            // 将resultInfo对象设置到request作用域中
            request.setAttribute("resultInfo", resultInfo.getMsg());
            // 请求转发跳到注册页面
            request.getRequestDispatcher("regist.jsp").forward(request, response);
        }
    }
    /**
     * 用户登录
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void userLogin(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        //接收表单信息
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String verifyc  = request.getParameter("verifycode");
        //设置回显
        request.setAttribute("username", username);
        request.setAttribute("password", password);
        request.setAttribute("verifycode", verifyc);
        //获取验证码
        String svc =(String) request.getSession().getAttribute("sessionverify");
        //验证码验证
        if(!svc.equalsIgnoreCase(verifyc)){
            request.setAttribute("resultInfo", "验证码错误！");
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }
        //调用Service层的方法，返回ResultInfo对象
        ResultInfo<User> resultInfo = userService.userLogin(username, password);
        //判断是否登录成功
        if (resultInfo.getCode() == 1) { // 如果成功
            //将用户信息设置到session作用域中
            request.getSession().setAttribute("user", resultInfo.getResult());
            //重定向跳转到index页面
            response.sendRedirect("index"); //通过重定向IndexServlet控制器，设置请求域参数后，动态显示index.jsp右边的页面
        } else {
            //将resultInfo对象设置到request作用域中
            request.setAttribute("resultInfo", resultInfo.getMsg());
            //请求转发跳到登录页面
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }

    }
}
