package com.jun.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.jun.dao.UserDao;
import com.jun.pojo.User;
import com.jun.vo.ResultInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

/**
 * @title:UserService
 * @Author Jun
 * @Date:2023/1/7 19:29
 * @Version 1.0
 */
public class UserService {
    UserDao userDao = new UserDao();

    /**
     *  用户注册
     * @param userName
     * @param userPassword
     * @return
     */
    public ResultInfo<User> userRegister(String userName, String userPassword) {
        ResultInfo<User> resultInfo = new ResultInfo<>();

        User u = new User();
        u.setUname(userName);
        u.setUpwd(userPassword);
        resultInfo.setResult(u);
        // 1.判断参数是否为空, StrUtil工具类，是从 hutool工具类 依赖中来的
        if (StrUtil.isBlank(userName) || StrUtil.isBlank(userPassword)) {
            // 如果为空，设置ResultInfo对象的状态码和提示信息
            resultInfo.setCode(0);
            resultInfo.setMsg("用户名或密码不能为空！");
            // 返回 resultInfo 对象
            return resultInfo;
        }
        // 2.通过用户名查询用户对象
        User user = userDao.selectByName(userName);
        // 3.判断用户对象是否为空
        if (user == null) {
            //将前端的数据通过md5加密存到数据库
            String insertPassword = DigestUtil.md5Hex(userPassword);
            int num = userDao.insertUser(userName, insertPassword);
            if (num > 0) {
                resultInfo.setCode(1);
            } else {
                resultInfo.setCode(0);
                resultInfo.setMsg("注册失败未知错误！");
            }
            return resultInfo;
        }
        // 4.如果不为空，设置ResultInfo对象的状态码和提示信息
        resultInfo.setCode(0);
        resultInfo.setMsg("该用户已存在！");
        // 5.返回 resultInfo 对象
        return resultInfo;
    }
    /**
     * 用户登录
     * @param userName
     * @param userPassword
     * @return
     */
    public ResultInfo<User> userLogin(String userName,String userPassword) {
        ResultInfo<User> resultInfo = new ResultInfo<>();

        User u = new User();
        u.setUname(userName);
        u.setUpwd(userPassword);

        resultInfo.setResult(u);
        // 1.判断参数是否为空, StrUtil工具类，是从 hutool工具类 依赖中来的
        if (StrUtil.isBlank(userName) || StrUtil.isBlank(userPassword)) {
            // 如果为空，设置ResultInfo对象的状态码和提示信息
            resultInfo.setCode(0);
            resultInfo.setMsg("用户名或密码不能为空！");
            // 返回 resultInfo 对象
            return resultInfo;
        }
        // 2.如果不为空，通过用户名查询用户对象
        User user = userDao.selectByName(userName);
        // 3.判断用户对象是否为空
        if (user == null) {
            // 如果为空，设置ResultInfo对象的状态码和提示信息
            resultInfo.setCode(0);
            resultInfo.setMsg("该用户不存在！");
            // 返回 resultInfo 对象
            return resultInfo;
        }
        // 4. 如果用户对象不为空，将数据库中查询到的用户对象的密码与前台传递的密码作比较 （将密码加密后再比较）
        // 将前台传递的密码按照MD5算法的方式加密
        userPassword = DigestUtil.md5Hex(userPassword); // 使用 hutool 工具类的加密方法
        // 判断加密后密码是否与数据库中的一致
        if (!userPassword.equals(user.getUpwd())) {
            // 如果密码不正确
            // 如果为空，设置ResultInfo对象的状态码和提示信息
            resultInfo.setCode(0);
            resultInfo.setMsg("用户密码不正确！");
            // 返回 resultInfo 对象
            return resultInfo;
        }
        // 如果用户名和密码都与数据库中的一致
        resultInfo.setCode(1);
        resultInfo.setResult(user);
        return resultInfo;

    }
    /**
     * 验证昵称的唯一性
     * @param nick
     * @param userId
     * @return
     */
    public Integer checkNick(String nick, Integer userId) {
        //1.判断昵称是否为空
        if (StrUtil.isBlank(nick)) {
            return 0;
        }
        //2.调用dao层
        User user = userDao.selectByNickAndUserId(nick, userId);
        //3.判断用户对象存在
        if (user != null) {
            return 0;
        }
        return 1;
    }
    /**
     * 更新用户信息
     * @param request
     * @return
     */
    public ResultInfo<User> updateUser(HttpServletRequest request){
        ResultInfo<User> resultInfo = new ResultInfo<>();
        //1.获取参数
        String nick = request.getParameter("nick");
        String mood = request.getParameter("mood");
        //2.参数非空判断
        if (StrUtil.isBlank(nick)) {
            resultInfo.setCode(0);
            resultInfo.setMsg("用户昵称不能为空！");
            return resultInfo;
        }
        //3.从session作用域中获取用户对象
        User user = (User) request.getSession().getAttribute("user");
        //4.设置修改的昵称和心情
        user.setNick(nick);
        user.setMood(mood);
        //5.实现文件上传
        try {
            //取Part对象
            Part part = request.getPart("img");
            //通过Part对象获取上传文件名
            String header = part.getHeader("Content-Disposition");
            //获取具体的请求头对应的值
            String str = header.substring(header.lastIndexOf("=") + 2);
            //获取上传文件名
            String fileName = str.substring(0, str.length() - 1);
            //判断文件名是否为空
            if (!StrUtil.isBlank(fileName)) {
                //获取更新的头像
                user.setHead(fileName);
                //获取文件存放的路径
                String filePath = request.getServletContext().getRealPath("/WEB-INF/upload");
                //上传文件到指定目录
                part.write(filePath + "/" + fileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //6.调用dao层更新
        int row = userDao.updateUser(user);
        //7.判断结果集
        if (row > 0) {
            resultInfo.setCode(1);
            //更新session作用域
            request.getSession().setAttribute("user", user);
        } else {
            resultInfo.setCode(0);
            resultInfo.setMsg("更新失败！");
        }
        //8.返回结果集
        return resultInfo;
    }
    /**
     * 忘记密码
     * @param request
     * @return
     */
    public ResultInfo<User> forgetUser(HttpServletRequest request) {
        ResultInfo<User> resultInfo = new ResultInfo<>();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        //1.判空
        if (StrUtil.isBlank(username) || StrUtil.isBlank(password)) {
            resultInfo.setCode(0);
            resultInfo.setMsg("用户名或修改密码为空！");
        }
        //2.调用dao层方法,判断用户是否合法
        User user = userDao.selectByName(username);
        if (user == null) {
            resultInfo.setCode(0);
            resultInfo.setMsg("该用户名不存在！");
        }
        //3.将修改后的密码md5加密后，调用dao层方法更新
        String updatePassword = DigestUtil.md5Hex(password);
        if (user.getUpwd().equals(updatePassword)) {
            resultInfo.setCode(0);
            resultInfo.setMsg("修改后的密码与原密码相同！");
            return resultInfo;
        } else {
            int num = userDao.updateByUsername(username, updatePassword);
            //4.判断num
            if (num > 0) {
                resultInfo.setCode(1);
            } else {
                resultInfo.setCode(0);
                resultInfo.setMsg("修改失败未知错误！");
            }
        }
        //5.返回结果集
        return resultInfo;
    }
}
