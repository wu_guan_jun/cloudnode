package com.jun.dao;

import com.jun.pojo.User;
import java.util.ArrayList;
import java.util.List;


/**
 * @title:UserDao
 * @Author Jun
 * @Date:2023/1/7 19:29
 * @Version 1.0
 */
public class UserDao {
    /**
     * 新增用户
     * @param userName
     * @param userPassword
     * @return
     */
    public int insertUser(String userName, String userPassword) {
        //定义sql
        String sql = "INSERT INTO tb_user(uname,upwd) VALUES(?,?);";
        //设置参数集合
        List<Object> params = new ArrayList<>();
        params.add(userName);
        params.add(userPassword);
        //调用BaseDao的更新方法
        int num = BaseDao.executeUpdate(sql, params);
        return num;
    }

    /**
     * 通过用户名查询用户对象
     * @param userName
     * @return
     */
    public User selectByName(String userName) {
        //定义sql
        String sql = "SELECT * FROM tb_user WHERE uname =?;";
        //设置参数集合
        List<Object> params = new ArrayList<>();
        params.add(userName);
        //调用BaseDao的查询方法
        User user = (User) BaseDao.queryRow(sql, params, User.class);
        return user;
    }

    /**
     * 通过用户昵称和用户id查询用户对象
     * @param nick
     * @param userId
     * @return
     */
    public User selectByNickAndUserId(String nick, Integer userId) {
        //定义sql
        String sql = "SELECT * FROM tb_user WHERE nick =? AND userId !=?;";
        //设置参数集合
        List<Object> params = new ArrayList<>();
        params.add(nick);
        params.add(userId);
        //调用BaseDao的查询方法
        User user = (User) BaseDao.queryRow(sql, params, User.class);
        return user;
    }

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    public int updateUser(User user) {
        //定义sql
        String sql = "UPDATE tb_user SET nick =?,mood=?,head=? WHERE userId =?;";
        //设置参数集合
        List<Object> params = new ArrayList<>();
        params.add(user.getNick());
        params.add(user.getMood());
        params.add(user.getHead());
        params.add(user.getUserId());
        //调用BaseDao的更新方法
        int num = BaseDao.executeUpdate(sql, params);
        return num;
    }

    /**
     * 忘记密码
     * @param userName
     * @param updatePassword
     * @return
     */
    public int updateByUsername(String userName, String updatePassword) {
        //定义sql
        String sql = "UPDATE tb_user SET upwd=? WHERE uname =?;";
        //设置参数集合
        List<Object> params = new ArrayList<>();
        params.add(updatePassword);
        params.add(userName);
        int num = BaseDao.executeUpdate(sql, params);
        return num;
    }
}
