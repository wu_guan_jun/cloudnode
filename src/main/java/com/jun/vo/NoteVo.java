package com.jun.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * @title:NoteVo
 * @Author Jun
 * @Date:2023/2/22 14:12
 * @Version 1.0
 */
@Getter
@Setter
public class NoteVo {
    private String groupName; // 分组名称
    private long noteCount; // 云记数量

    private Integer typeId; // 类型Id
}
