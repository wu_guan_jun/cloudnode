package com.jun.filter;

import cn.hutool.core.util.StrUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @title:EncodingFilter
 * @Author Jun
 * @Date:2023/1/9 14:38
 * @Version 1.0
 */
@WebFilter("/*")    //过滤所有资源
//请求乱码解决
public class EncodingFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 过滤器的初始化方法
    }

    @Override
    public void destroy() {
        // 过滤器销毁时的方法
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // 转一下，基于HTTP
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        // 处理POST请求（只针对POST请求有效，GET请求不受影响）
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");//当前页
        response.setCharacterEncoding("UTF-8");//跳转页
        // 处理GET
        // 得到请求类型（GET|POST）
        String method = request.getMethod();

        // 如果是GET请求，则判断服务器版本
        if ("GET".equalsIgnoreCase(method)) { // 忽略大小写比较
            // 得到服务器版本，Apache Tomcat/7.0.79
            String serverInfo = request.getServletContext().getServerInfo();
            // 通过截取字符串，得到具体的版本号，从第一个 / 开始，不包含 / 所以 +1，到 . 结束
            String version = serverInfo.substring(serverInfo.lastIndexOf("/") + 1, serverInfo.indexOf("."));
            // 判断服务器版本是否是Tomcat7及以下
            if (version != null && Integer.parseInt(version) < 8) {
                // Tomcat7及以下版本的服务器的GET请求
                MyWrapper myRequest = new MyWrapper(request); // 生成下面的内部类对象，传入request参数
                // 放行资源
                filterChain.doFilter(myRequest, response);
                // return一下，不让他往后面走了
                return;
            }
        }
        // 过滤器放行
        filterChain.doFilter(request, response);
    }

    /**
     * 内部类
     * <p>
     * 1、定义内部类（类的本质是 request 对象）
     * 2、继承 HttpServletRequestWrapper 包装类
     * 3、重写 getParameter()方法
     */
    class MyWrapper extends HttpServletRequestWrapper { // HttpServletRequestWrapper 本质就是 HttpServletRequest，看他的继承关系就能知道
        // 定义成员变量 HttpServletRequest（作用：提升构造器中 request 对象的作用域）
        private HttpServletRequest request;

        /**
         * 带参构造
         * --可以得到需要处理的request对象
         *
         * @param request
         */
        public MyWrapper(HttpServletRequest request) {
            super(request);
            this.request = request; // 初始化成员变量
        }

        /**
         * 重写 getParameter()方法，处理乱码问题
         *
         * @param name
         * @return
         */
        @Override
        public String getParameter(String name) {
            // 获取参数（会乱码的参数）
            String value = request.getParameter(name);
            // 判断参数值是否为空，使用hutool工具判空
            if (StrUtil.isBlank(value)) { // 为空时
                return value;
            }
            try {
                // 通过 new String() 处理乱码
                value = new String(value.getBytes("ISO-8859-1"), "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return value;
        }
    }
}
