package com.jun.filter;

import com.jun.pojo.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @title:LoginFilter
 * @Author Jun
 * @Date:2023/1/9 15:18
 * @Version 1.0
 */
@WebFilter("/*")
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * 拦截器
     * @param servletRequest
     * @param servletResponse
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        //基于HTTP
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //等到访问的路径
        String path = request.getRequestURI();    //格式：项目路径/资源路径
        //指定页面放行
        if (path.contains("/login.jsp")) {
            chain.doFilter(request, response);
            return;
        }
        if (path.contains("/regist.jsp")) {
            chain.doFilter(request, response);
            return;
        }
        //静态资源放行
        if (path.contains("/static")) {
            chain.doFilter(request, response);
            return;
        }
        if (path.contains("/VerifyCodeServlet")) {
            chain.doFilter(request, response);
            return;
        }
        if (path.contains("/forget.jsp")) {
            chain.doFilter(request, response);
            return;
        }
        //指定行为放行，登录注册
        if (path.contains("/user")) {
            //得到用户行为
            String actionName = request.getParameter("actionName");
            //判断是否是登录操作
            if ("login".equals(actionName)) {
                chain.doFilter(request, response);
                return;
            }
            //判断是否是注册操作
            if ("register".equals(actionName)) {
                chain.doFilter(request, response);
                return;
            }
            //判断是否是忘记密码
            if ("forget".equals(actionName)) {
                chain.doFilter(request, response);
                return;
            }
        }
        //登录状态，放行
        //获取session作用域中的用户对象
        User user = (User) request.getSession().getAttribute("user");
        //判断user对象是否为空
        if (user != null) {
            chain.doFilter(request, response);
            return;
        }
        //拦截请求，重定向跳转到登录页面
        response.sendRedirect("login.jsp");
    }

    @Override
    public void destroy() {

    }
}
